# house of flames

|attribute|value|meaning|
|---|---|---|
|color|red|loyalty, wisdom, trust|
|stone|ruby|concentration, motivation|
|symbol|circles|power through ritual|

## motto

> this body is a temple
>
> not a prison
> 
> free yourself
> 
> from your mind

## known for
* rituals
* chanting
* loud song
* rigorous ritual training 💪

> more information coming soon!

---

🌟

---

<a rel="license" href="http://creativecommons.org/licenses/by-nc-nd/4.0/"><img alt="Creative Commons License" style="border-width:0" src="https://i.creativecommons.org/l/by-nc-nd/4.0/88x31.png" /></a><br /><span xmlns:dct="http://purl.org/dc/terms/" property="dct:title">eden</span> by <a xmlns:cc="http://creativecommons.org/ns#" href="https://tacowolf.net/eden" property="cc:attributionName" rel="cc:attributionURL">daniel alejandro gallegos</a> is licensed under a <a rel="license" href="http://creativecommons.org/licenses/by-nc-nd/4.0/">Creative Commons Attribution-NonCommercial-NoDerivatives 4.0 International License</a>.
