# house of stones

|attribute|value|meaning|
|---|---|---|
|color|green|renewal, nature, energy|
|stone|jade|abundance, nourishment|
|symbol|diamond|divinity in nature|

## motto

> sticks and stones
>
> may break bones
> 
> but their words
>
> shall never hurt me.

## known for
* building
* architecture
* divine inspiration
* intricate puzzles
* complicated designs
* meditation 🧘🏻

> more information coming soon!

---

🌟

---

<a rel="license" href="http://creativecommons.org/licenses/by-nc-nd/4.0/"><img alt="Creative Commons License" style="border-width:0" src="https://i.creativecommons.org/l/by-nc-nd/4.0/88x31.png" /></a><br /><span xmlns:dct="http://purl.org/dc/terms/" property="dct:title">eden</span> by <a xmlns:cc="http://creativecommons.org/ns#" href="https://tacowolf.net/eden" property="cc:attributionName" rel="cc:attributionURL">daniel alejandro gallegos</a> is licensed under a <a rel="license" href="http://creativecommons.org/licenses/by-nc-nd/4.0/">Creative Commons Attribution-NonCommercial-NoDerivatives 4.0 International License</a>.
