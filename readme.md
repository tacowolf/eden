# eden
> a world we'll make together.

## table of contents

### houses

* [house of streams](houses/streams)
* [house of leaves](houses/leaves)
* [house of flames](houses/flames)
* [house of stones](houses/stones)

### reports

* [on the space time continuum :: akino scio - 1201958](reports/1201958)

---

🌟

---

<a rel="license" href="http://creativecommons.org/licenses/by-nc-sa/4.0/"><img alt="Creative Commons License" style="border-width:0" src="https://i.creativecommons.org/l/by-nc-sa/4.0/88x31.png" /></a><br /><span xmlns:dct="http://purl.org/dc/terms/" href="http://purl.org/dc/dcmitype/Text" property="dct:title" rel="dct:type">eden</span> by <a xmlns:cc="http://creativecommons.org/ns#" href="https://daniel.ga/llegos" property="cc:attributionName" rel="cc:attributionURL">daniel alejandro gallegos</a> is licensed under a <a rel="license" href="http://creativecommons.org/licenses/by-nc-sa/4.0/">Creative Commons Attribution-NonCommercial-ShareAlike 4.0 International License</a>.
<!--stackedit_data:
eyJoaXN0b3J5IjpbLTk5NDgzODgxNV19
-->